/**********************************************************************************
 * Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#pragma once

#include "expression_utils/expression_utils_get_shape.hpp"
#include "expression_utils/expression_utils_is_constant.hpp"
#include "expression_utils/expression_utils_replace_constant.hpp"
#include "expression_utils/expression_utils_replace_parameter.hpp"



